#! /bin/sh

# Path to project root build script (CMakeLists.txt)
scriptdir=$(pwd)/$(dirname $0)

build_type="Default"
sanitize="-DSANITIZE=OFF"
coverage="-DCOVERAGE=OFF"

while getopts "hdrsc" opts
do
case $opts in
    "h")
        echo ""
        echo "Configuration menu. Configure build by type command as ./config.sh <args> where:"
        echo "    d - add debug flags"
        echo "    r - add release flags"
        echo "    s - add sanitize flags"
        echo "    c - add coverage flags"
        echo ""
        exit
        ;;
    "d")
        build_type="Debug"
        ;;
    "r")
        build_type="Release"
        ;;
    "s")
        #cxx_flags="$cxx_flags -fsanitize=address -static-libasan -fsanitize=leak -static-liblsan -fsanitize=undefined -static-libubsan"
        cxx_flags="$cxx_flags -fsanitize=address -fsanitize=leak -fsanitize=undefined"
        sanitize="-DSANITIZE=ON"
        ;;
    "c")
        cxx_flags="$cxx_flags --coverage"
        coverage="-DCOVERAGE=ON"
        ;;
    *)
        echo ""
        exit
        ;;
esac
done

cmake $scriptdir -DCMAKE_INSTALL_PREFIX=out -DCMAKE_BUILD_TYPE=$build_type $sanitize $coverage -DCMAKE_CXX_FLAGS="$cxx_flags"
