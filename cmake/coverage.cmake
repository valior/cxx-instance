set(TARGET_NAME coverage)

find_program(LCOV lcov)

if(${LCOV} STREQUAL LCOV-NOTFOUND)
    message(${LCOV})
else()
    execute_process(COMMAND lcov --version OUTPUT_VARIABLE OUTPUT OUTPUT_STRIP_TRAILING_WHITESPACE)
    message(STATUS ${OUTPUT})

    add_custom_target(${TARGET_NAME}
        WORKING_DIRECTORY ${CMAKE_BINARY_DIR}
        COMMAND lcov -c -d . -o ${CMAKE_PROJECT_NAME}.info
        COMMAND lcov -r ${CMAKE_PROJECT_NAME}.info '/usr*' '*gtest*' -o ${CMAKE_PROJECT_NAME}.info
        COMMAND genhtml ${CMAKE_PROJECT_NAME}.info -o coverage
    )

    if(TARGET check)
        add_dependencies(${TARGET_NAME} check)
    endif()
endif()
