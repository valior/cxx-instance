set(TARGET_NAME cppcheck)

find_program(CPPCHECK cppcheck)

if(${CPPCHECK} STREQUAL CPPCHECK-NOTFOUND)
    message(${CPPCHECK})
else()
    execute_process(COMMAND cppcheck --version OUTPUT_VARIABLE OUTPUT OUTPUT_STRIP_TRAILING_WHITESPACE)
    message(STATUS ${OUTPUT})

    add_custom_target(${TARGET_NAME}-${PROJECT_NAME}
        WORKING_DIRECTORY ${CMAKE_CURRENT_SOURCE_DIR}
        COMMAND cppcheck --std=c++11 --enable=all --platform=unix64 --suppress=missingIncludeSystem
                --inconclusive -I ../include ${project_sources} ${tests_sources}
    )

    if(TARGET ${TARGET_NAME})
        add_dependencies(${TARGET_NAME} ${TARGET_NAME}-${PROJECT_NAME})
    endif()
endif()
