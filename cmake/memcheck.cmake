set(TARGET_NAME memcheck)

find_program(VALGRIND valgrind)

if(${VALGRIND} STREQUAL VALGRIND-NOTFOUND)
    message(${VALGRIND})
else()
    execute_process(COMMAND valgrind --version OUTPUT_VARIABLE OUTPUT OUTPUT_STRIP_TRAILING_WHITESPACE)
    message(STATUS ${OUTPUT})

    add_custom_target(${TARGET_NAME}-${PROJECT_NAME}
        WORKING_DIRECTORY ${CMAKE_CURRENT_BINARY_DIR}
        COMMAND valgrind --tool=memcheck --leak-check=full --error-exitcode=1 ./${PROJECT_NAME} $(ARGS)
        DEPENDS ${PROJECT_NAME}
    )

    if(TARGET ${TARGET_NAME})
        add_dependencies(${TARGET_NAME} ${TARGET_NAME}-${PROJECT_NAME})
    endif()
endif()
