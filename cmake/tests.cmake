set(TARGET_NAME tests)

if(TARGET ${TARGET_NAME})
    add_dependencies(${TARGET_NAME} ${PROJECT_NAME})
endif()
