set(TARGET_NAME vera)

find_program(VERA vera++)

if(${VERA} STREQUAL VERA-NOTFOUND)
    message(${VERA})
else()
    execute_process(COMMAND vera++ --version OUTPUT_VARIABLE OUTPUT OUTPUT_STRIP_TRAILING_WHITESPACE)
    message(STATUS "vera++ ${OUTPUT}")

    add_custom_target(vera-${PROJECT_NAME}
        WORKING_DIRECTORY ${CMAKE_CURRENT_SOURCE_DIR}
        COMMAND vera++ ${project_sources} ${tests_sources}
    )

    if(TARGET ${TARGET_NAME})
        add_dependencies(${TARGET_NAME} ${TARGET_NAME}-${PROJECT_NAME})
    endif()
endif()
