#ifndef ARITH_H
#define ARITH_H

double sum(double a, double b);
double sub(double a, double b);

double multiply(double a, double b);
double divide(double a, double b);

#endif // ARITH_H
