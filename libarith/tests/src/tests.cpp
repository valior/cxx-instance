#include <gtest/gtest.h>
#include <arith.h>

TEST(Arith, SumTest_Success)
{
    ASSERT_EQ(4, sum(2, 2));
    ASSERT_EQ(7, sum(2, 5));
}

TEST(Arith, SubTest_Success)
{
    ASSERT_EQ(7, sub(10, 3));
    ASSERT_EQ(8, sub(15, 7));
}

TEST(Arith, MultiplyTest_Success)
{
    ASSERT_EQ(4, multiply(2, 2));
    ASSERT_EQ(100, multiply(10, 10));
}

TEST(Arith, DivideTest_Success)
{
    ASSERT_EQ(2, divide(4, 2));
    ASSERT_EQ(10, divide(100, 10));
}
